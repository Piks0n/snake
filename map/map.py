import random

from map.entities.food import Food, FoodGenerator
from utils.dimensions import Dimensions


class MapField:
    FIELD_EMPTY = 0
    FIELD_FOOD = 1
    FIELD_SNAKE = 2

    def __init__(self, field_type: int=FIELD_EMPTY):
        if 0 > field_type > 2:
            raise ValueError("Supported field types are: 0 - empty, 1 - food, 2 - snake!")
        self.__type = field_type
        self.__food = None

    def set_empty(self):
        self.__type = MapField.FIELD_EMPTY
        self.__food = None

    def set_food(self, food: Food):
        self.__type = MapField.FIELD_FOOD
        self.__food = food

    def set_snake(self):
        self.__type = MapField.FIELD_SNAKE
        self.__food = None

    def is_empty(self):
        return self.__type is MapField.FIELD_EMPTY

    def is_food(self):
        return self.__type is MapField.FIELD_FOOD

    def is_snake(self):
        return self.__type is MapField.FIELD_SNAKE

    def get_points(self):
        if self.is_food():
            return self.__food.points
        else:
            return 0

    def get_food(self) -> Food:
        return self.__food


class GameMap:
    def __init__(self, dimensions: Dimensions):
        self.__dimensions = dimensions
        self.width = self.__dimensions.width
        self.height = self.__dimensions.height
        self.__fields = self.__generate_empty_fields()
        self.__generate_empty_fields()
        self.food_generator = FoodGenerator()

    def __generate_empty_fields(self) -> list:
        result = [None] * self.width
        for x in range(0, self.width):
            result[x] = [None] * self.height
            for y in range(0, self.height):
                result[x][y] = MapField()
        return result

    def get_field(self, x: int, y: int) -> MapField:
        if self.are_coordinates_in_map(x, y):
            return self.__fields[x][y]

        else:
            raise ValueError("Coordinates out of range!")

    def are_coordinates_in_map(self, x: int, y: int) -> bool:
        return 0 <= x < self.__dimensions.width and 0 <= y < self.__dimensions.height

    def generate_food(self) -> None:
        new_food = self.food_generator.generate_food()

        empty_fields_amount = self.count_empty_fields()
        if empty_fields_amount == 0:
            return

        random_index = random.randint(1, empty_fields_amount)

        counter = 0
        for row in self.__fields:
            for field in row:
                if field.is_empty():
                    counter += 1
                if counter == random_index:
                    field.set_food(new_food)
                    return

        raise RuntimeError("Random food generation error!")

    def count_empty_fields(self) -> int:
        counter = 0
        for row in self.__fields:
            for field in row:
                if field.is_empty():
                    counter += 1
        return counter
