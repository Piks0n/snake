from enum import Enum
import random


class FoodType(Enum):
    NORMAL = 10
    SPECIAL = 100


class Food:
    def __init__(self, food_type: FoodType=FoodType.NORMAL):
        self.points = food_type.value


class FoodGenerator:
    def generate_food(self) -> Food:
        rand = random.randint(0, 100)

        if rand < 5:
            return Food(FoodType.SPECIAL)
        else:
            return Food(FoodType.NORMAL)
