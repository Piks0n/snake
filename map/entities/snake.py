from enum import Enum
from map.entities.food import Food

from map.map import GameMap, MapField
from interface.keyboard import *
from utils.color import Color, ColorUtils


class SnakeElement:
    def __init__(self, x: int, y: int, is_head: bool = False):
        self.is_head = is_head
        self.x = x
        self.y = y


class MovementDirection(Enum):
    NORTH = 1
    EAST = 2
    SOUTH = 3
    WEST = 4


class Snake:
    def __init__(self, name: str, color: Color, x: int, y: int, key_settings: KeySettings):
        self.name = name
        self.__body = [SnakeElement(x, y, True)]
        self.__alive = True
        self.points = 0
        self.__key_settings = key_settings
        self.__direction = MovementDirection.NORTH
        self.color_block = ColorUtils.get_color_block(color)
        self.__last_tick = 0

    def get_elements_positions(self) -> tuple:
        positions = list()
        for element in self.__body:
            positions.append((element.x, element.y))

        return positions

    def on_key_pressed(self, key: int, current_tick: int) -> None:
        if key == self.__key_settings.north:
            self.__set_direction(MovementDirection.NORTH, current_tick)
        elif key == self.__key_settings.south:
            self.__set_direction(MovementDirection.SOUTH, current_tick)
        elif key == self.__key_settings.west:
            self.__set_direction(MovementDirection.WEST, current_tick)
        elif key == self.__key_settings.east:
            self.__set_direction(MovementDirection.EAST, current_tick)

    def __set_direction(self, direction: MovementDirection, current_tick: int) -> None:
        if self.__last_tick == current_tick:
            return
        else:
            self.__last_tick = current_tick

        if direction == MovementDirection.NORTH and self.__direction == MovementDirection.SOUTH:
            return
        elif direction == MovementDirection.SOUTH and self.__direction == MovementDirection.NORTH:
            return
        elif direction == MovementDirection.WEST and self.__direction == MovementDirection.EAST:
            return
        elif direction == MovementDirection.EAST and self.__direction == MovementDirection.WEST:
            return

        self.__direction = direction

    def move(self, game_map: GameMap) -> None:
        if not self.is_alive():
            return

        head = self.__body[0]

        new_x = head.x
        new_y = head.y

        if self.__direction is MovementDirection.NORTH:
            new_y -= 1
        elif self.__direction is MovementDirection.EAST:
            new_x += 1
        elif self.__direction is MovementDirection.SOUTH:
            new_y += 1
        elif self.__direction is MovementDirection.WEST:
            new_x -= 1
        else:
            raise ValueError("Invalid movement direction!")

        if not game_map.are_coordinates_in_map(new_x, new_y):
            self.die(game_map)
            return

        destination_field = game_map.get_field(new_x, new_y)

        food_to_eat = None
        if destination_field.is_food():
            food_to_eat = destination_field.get_food()

        tail = self.__body[len(self.__body) - 1]
        # Jeżeli głowa wchodzi na snake'a i ten snake nie jest końcówką ogona (zapętlenie)
        if destination_field.is_snake():
            self.die(game_map)
            return

            # if new_x is not tail.x and new_y is not tail.y:
            #     self.die(game_map)
            #     return
            # else:
            #     self.__print_debug_info("Własny ogon?")

        for element in self.__body:
            old_x = element.x
            old_y = element.y
            element.x = new_x
            element.y = new_y
            game_map.get_field(new_x, new_y).set_snake()
            game_map.get_field(old_x, old_y).set_empty()
            new_x = old_x
            new_y = old_y

        if food_to_eat is not None:
            self.__grow(new_x, new_y, game_map, food_to_eat)

        game_map.get_field(head.x, head.y).set_snake()

    def die(self, game_map: GameMap) -> None:
        self.__alive = False

        for element in self.__body:
            game_map.get_field(element.x, element.y).set_empty()

    def __grow(self, x: int, y: int, game_map: GameMap, food_to_eat: Food) -> None:
        self.points += food_to_eat.points
        self.__body.append(SnakeElement(x, y))
        game_map.get_field(x, y).set_snake()

    def is_alive(self):
        return self.__alive

    def __print_debug_info(self, text: str = ""):
        print("Debug " + text)
        print("Snake = " + self.name)
        for element in self.__body:
            print("x = " + str(element.x) + "\ty = " + str(element.y))
