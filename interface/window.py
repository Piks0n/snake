import pygame
from map.map import GameMap
from utils.dimensions import Dimensions


class Window:
    __BLOCK_COLOR_BLUE = (0, 0, 255)
    __BLOCK_COLOR_BLACK = (0, 0, 0)

    def __init__(self, dimensions: Dimensions):
        self.__dimensions = dimensions
        self.__food_block = self.__get_block(Window.__BLOCK_COLOR_BLUE)
        self.__window = pygame.display.set_mode(self.__dimensions.as_scaled_tuple())
        pygame.display.set_caption("Snake KK")

    def __get_block(self, color: tuple) -> pygame.Surface:
        square = pygame.Surface((self.__dimensions.scale, self.__dimensions.scale))
        square.set_alpha(255)
        square.fill(color)
        return square

    def display(self, game_map: GameMap, snakes: list):
        self.__window.fill(Window.__BLOCK_COLOR_BLACK)

        for x in range(0, game_map.width):
            for y in range(0, game_map.height):
                field = game_map.get_field(x, y)
                # zarcie
                if field.is_food():
                    self.display_square(self.__food_block, x, y)

        for snake in snakes:
            for position in snake.get_elements_positions():
                self.display_square(snake.color_block, position[0], position[1])

        pygame.display.flip()

    def display_text(self, lines: list):
        self.__window.fill(Window.__BLOCK_COLOR_BLACK)

        my_font = pygame.font.SysFont("monospace", 15)

        y_position = 0
        for text in lines:
            label = my_font.render(text, 1, (255, 255, 0))
            self.__window.blit(label, (0, y_position))
            y_position += my_font.get_height() + 5

        pygame.display.flip()

    def display_square(self, square: pygame.Surface, x: int, y: int):
        scale = self.__dimensions.scale
        self.__window.blit(square, (int(x * scale), int(y * scale)))
