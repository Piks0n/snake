import json
from collections import Counter


class KeySettings:
    def __init__(self, north: int, south: int, west: int, east: int):
        self.__validate([north, south, west, east])

        self.north = north
        self.south = south
        self.west = west
        self.east = east

    def __validate(self, key_list: list):
        if len([k for k, v in Counter(key_list).items() if v > 1]) != 0:
            raise ValueError("Keys are duplicated.")

    def to_json(self):
        return json.dumps(self.__dict__)

    @classmethod
    def from_json(cls, json_string: str):
        json_dict = json.loads(json_string)
        try:
            result = KeySettings(**json_dict)
        except TypeError as e:
            raise ValueError("Json is invalid.")
