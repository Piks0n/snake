from utils.dimensions import Dimensions
import pygame


class Color:
    def __init__(self, red: int, green: int, blue: int):
        self.red = red
        self.green = green
        self.blue = blue


class ColorUtils:
    BLOCK_SIZE = 0

    @classmethod
    def set_block_size(cls, dimensions: Dimensions) -> None:
        cls.BLOCK_SIZE = dimensions.scale

    @classmethod
    def get_color_block(cls, color: Color) -> pygame.Surface:
        square = pygame.Surface((cls.BLOCK_SIZE, cls.BLOCK_SIZE))
        square.set_alpha(255)
        square.fill(cls.__translate_color(color))
        return square

    @classmethod
    def __translate_color(cls, color: Color) -> tuple:
        return color.red, color.green, color.blue
