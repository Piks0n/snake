class Dimensions:
    def __init__(self, width: int, height: int, scale: float=1.0):
        self.width = width
        self.width_scaled = int(width * scale)
        self.height = height
        self.height_scaled = int(height * scale)
        self.scale = scale

    def as_tuple(self) -> tuple:
        return self.width, self.height

    def as_scaled_tuple(self) -> tuple:
        return self.width_scaled, self.height_scaled
