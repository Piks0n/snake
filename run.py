﻿import pygame

from game.manager import GameManager
from interface.keyboard import KeySettings
from utils.dimensions import Dimensions
from map.entities.snake import Snake
from utils.color import ColorUtils, Color

pygame.init()
game_speed = 8

while True:
    dimensions = Dimensions(30, 20, 25)
    ColorUtils.set_block_size(dimensions)

    snake_a = Snake("WążA", Color(145, 0, 80), 10, 15,
                    KeySettings(pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT))
    snake_b = Snake("WążB", Color(255, 200, 120), 5, 10, KeySettings(pygame.K_w, pygame.K_s, pygame.K_a, pygame.K_d))

    game_manager = GameManager(8).add_snake(snake_a).add_snake(snake_b)

    game_manager.start_game(dimensions)
