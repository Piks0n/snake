from pygame.event import Event

from interface.window import Window
from map.entities.snake import Snake
from map.map import GameMap
import pygame
import sys
from utils.dimensions import Dimensions


class GameManager:
    def __init__(self, game_speed_initial: int):
        self.__game_map = None
        self.__snakes = list()
        self.__food_generation_time = 35
        self.__iteration_counter = 0
        self.__game_speed = game_speed_initial
        self.__clock = pygame.time.Clock()
        self.__game_finished = False
        self.__ticks = 0

    def add_snake(self, snake: Snake):
        self.__snakes.append(snake)
        return self

    def start_game(self, dimensions: Dimensions):
        window = Window(dimensions)
        self.__game_map = GameMap(dimensions)

        while not self.__game_finished:
            window.display(self.__game_map, self.__snakes)
            self.__clock.tick(self.__game_speed)
            self.__ticks += 1

            test_event = Event(2, {
                'scancode': 32,
                'mod': 0,
                'unicode': 'd',
                'key': 1100
                })
            pygame.event.post(test_event)

            while True:
                event = pygame.event.poll()

                if event.type == pygame.NOEVENT:
                    break

                elif event.type == pygame.QUIT:
                    sys.exit()

                elif event.type == pygame.KEYDOWN:
                    self.__on_key_pressed(event.key)

            self.__iterate()

            if self.__game_finished:
                self.after_game_finished(window)

    def __on_key_pressed(self, pressed_key: int) -> None:
        for snake in self.__snakes:
            snake.on_key_pressed(pressed_key, self.__ticks)

    def __iterate(self) -> None:
        self.__iteration_counter += 1

        if self.__iteration_counter == self.__food_generation_time:
            self.__game_map.generate_food()
            self.__iteration_counter = 0

        for snake in self.__snakes:
            snake.move(self.__game_map)

        self.__check_if_game_finished()

    def __check_if_game_finished(self):
        if self.__get_alive_snakes_amount() <= 1:
            self.__game_finished = True

    def __get_alive_snakes_amount(self) -> int:
        result = 0
        for snake in self.__snakes:
            if snake.is_alive():
                result += 1

        return result

    def after_game_finished(self, window: Window) -> None:
        winner_snake = self.__get_winner_snake()
        high_scores = self.__get_highscore_string_list()

        window.display_text(["Wygrywa " + winner_snake.name] + high_scores)
        pygame.time.delay(3500)

    def __get_winner_snake(self) -> Snake:
        winning_snake = self.__snakes[0]
        max_points = winning_snake.points

        for snake in self.__snakes:
            if snake.is_alive():
                return snake

            if snake.points > max_points:
                winning_snake = snake

        return winning_snake

    def __get_highscore_string_list(self) -> list:
        highscore_list = list()

        for snake in self.__snakes:
            highscore_string = snake.name
            highscore_string += "      "
            highscore_string += str(snake.points)
            highscore_list.append(highscore_string)

        return highscore_list


