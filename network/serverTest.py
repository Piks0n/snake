from __future__ import print_function

from gevent import _socket3

from gevent.server import StreamServer
from gevent import monkey

monkey.patch_thread()


class TestServer:
    __RUNNING_SERVERS = dict()

    def __init__(self, port: int):
        self.__server = StreamServer(('0.0.0.0', port), TestServer.__handle_connection)
        self.__port = port
        self.__is_running = False

    def serve_forever(self):
        if not self.__is_running:
            print("Starting server...")
            self.__is_running = True
            TestServer.__RUNNING_SERVERS[self.__port] = self
            self.__server.serve_forever()
        else:
            print("Server already running!")

    def kill(self):
        self.__server.stop()
        self.__is_running = False

    @staticmethod
    def __handle_connection(socket: _socket3.socket, address):
        print("Incoming connection from " + str(address))

        input_buffer = socket.makefile(mode='rb')

        TestServer.__check_request_matching(input_buffer, "start game")

        # TODO: przestać nasłuchiwać na inne rządania.

    @staticmethod
    def __check_request_matching(input_buffer, to_match: str) -> None:
        if input_buffer.readline() is not to_match:
            input_buffer.close()
            raise RuntimeError("Client sent invalid request.")


# this handler will be run for each incoming connection in a dedicated greenlet
def echo(socket, address):
    print('New connection from %s:%s' % address)
    socket.sendall(b'Welcome to the echo server! Type quit to exit.\r\n')
    # using a makefile because we want to use readline()
    rfileobj = socket.makefile(mode='rb')
    while True:
        line = rfileobj.readline()
        if not line:
            print("client disconnected")
            break
        if line.strip().lower() == b'quit':
            print("client quit")
            break
        socket.sendall(line)
        print("echoed %r" % line)
    rfileobj.close()


if __name__ == '__main__':
    server = TestServer(16000)

    server.serve_forever()
